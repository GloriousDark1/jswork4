"use strict";

function getNum() {
    let number = prompt("Вкажіть число",);
    while (number === null || number === "" || isNaN(number)) {
        number = prompt("Вкажіть число", number);
    }
    return +number;
}


function getOperator() {
    let operation = prompt("Вкажіть знак операції,(/,+,*,-)")
    while (
        operation !== '+' &&
        operation !== '-' &&
        operation !== '*' &&
        operation !== '/') {
        operation = prompt("Вкажіть знак операції,(/,+,*,-)", operation);
    }
    return operation;
}

let firstNum = getNum();
let secondNum = getNum();
let operator = getOperator();

function calc(a, b, c) {

    switch (c) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }

}

console.log(calc(firstNum, secondNum, operator));